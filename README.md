Yii2 AdminLTE Bootstrap Theme
=================
Theme for Yii2 Web Application

Installation
------------

```
php composer.phar require --prefer-dist kongoon/yii2-theme-adminlte "*"
```

or add 

```
"kongoon/yii2-theme-adminlte": "*"
```
to the require section of your composer.json file.

Usage
-----

Open your layout views/layouts/main.php and add

```
'components' => [
    'view' => [
         'theme' => [
             'pathMap' => [
                '@app/views' => '@vendor/kongoon/yii2-theme-adminlte/views/'
             ],
         ],
    ],
],

```
