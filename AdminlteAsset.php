<?php

/* 
 * 2015-02-20
 * @author Programmer Thailand <contact@programmerthailand>
 * http://www.programmerthailand.com
 * https://github.com/almasaeed2010/AdminLTE
 */
namespace kongoon\theme\adminlte;
use yii\web\AssetBundle;

class AdminlteAsset extends AssetBundle
{
    public $sourcePath = '@bower/admin-lte/dist';
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css'
    ];
    public $js = [
        'js/app.min.js'
    ];
    public $depends = [
        'rmrevin\yii\fontawesome\AssetBundle',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
